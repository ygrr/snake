package role;

import java.awt.Color;

/**
 * 蛇节点
 * @author Herrona
 *
 */
public class SnakeNode {
	public static int direction = 0;//方向
	public static final int UP = 0;
	public static final int RIGHT = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	private int x,y; //横纵坐标
	private int width=10,height=10;
	public Color color=Color.DARK_GRAY;
	
	public  SnakeNode(){}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}
	public void setColor(Color color) {
		this.color = color;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
}